var express = require('express');
var router = express.Router();

const MongoClient = require('mongodb').MongoClient;
const MongoObjectId = require('mongodb').ObjectID;
const assert = require('assert');

// Connection URL
const url = 'mongodb://localhost:27017';

// Database Name
const dbName = 'contact';

// Use connect method to connect to the server
// MongoClient.connect(url, function(err, client) {
//   assert.equal(null, err);
//   console.log("Connected successfully to server");

//   const db = client.db(dbName);

//   client.close();
// });

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

/* GET trang-chu page. */
router.get('/trang-chu', function(req, res, next) {  

  MongoClient.connect(url, function(err, client) {
    const findDocuments = function(db, callback) {
      // Get the documents collection
      const collection = db.collection('nguoidung');
      // Find some documents
      collection.find({}).toArray(function(err, docs) {
        assert.equal(err, null);        
        callback(docs);
      });
    }

    assert.equal(null, err);
    //console.log("Connected correctly to server");
  
    const db = client.db(dbName);
  
    findDocuments(db, function(danhsach) {
      res.render('trangchu', { title: 'Trang chủ', dulieu: danhsach });
      //console.log(danhsach);
      client.close();  
    });
  });
  
});

/* GET them page. */
router.get('/them', function(req, res, next) {
  res.render('them', { title: 'Thêm mới dữ liệu' });
});

/* POST them page. */
router.post('/them', function(req, res, next) {
  var ten = req.body.ten;
  var dulieu = {
    "ten": req.body.ten,
    "sdt" : req.body.sdt
  };

  const insertDocuments = function(db, callback) {
    // Get the documents collection
    const collection = db.collection('nguoidung');
    // Insert some documents
    collection.insert(dulieu, function(err, result) {
      assert.equal(err, null);      
      console.log("them du lieu thanh cong");
      callback(result);
    });
  }

  MongoClient.connect(url, function(err, client) {
    assert.equal(null, err);
    console.log("Connected successfully to server");
  
    const db = client.db(dbName);
  
    insertDocuments(db, function() {
      client.close();
    });
  });

  res.redirect('/trang-chu');

  // console.log(ten);
  // console.log("va so DT la " + req.body.sdt);
  //res.render('them', { title: 'Thêm mới dữ liệu' });
});

/* GET Xoa page. */
router.get('/xoa/:id', function(req, res, next) {
  var id = MongoObjectId(req.params.id);
  //lệnh xóa

  const removeContact = function(db, callback) {
    // Get the documents collection
    const collection = db.collection('nguoidung');
    // Delete document where a is 3
    collection.deleteOne({ _id : id }, function(err, result) {
      assert.equal(err, null);
      
      console.log("Xóa thành công");
      callback(result);     
    });
  }

  MongoClient.connect(url, function(err, client) {
    assert.equal(null, err);
    //console.log("Connected successfully to server");
  
    const db = client.db(dbName);
    
    removeContact(db, function() {
          client.close();   
          res.redirect('/trang-chu');
    });
  });
  
  //res.render('xoa', { title: 'Xóa' });
});


/* GET sua page. */
router.get('/sua/:id', function(req, res, next) {
  var id = MongoObjectId(req.params.id);
  
    const findContactById = function(db, callback) {
    // Get the documents collection
    const collection = db.collection('nguoidung');
    // Find some documents
    collection.find({_id: id}).toArray(function(err, docs) {
      assert.equal(err, null);
      console.log("Found the following records");
      //console.log(docs);
      callback(docs);
    });
  }
  
  MongoClient.connect(url, function(err, client) {
    assert.equal(null, err);
    const db = client.db(dbName);
    findContactById(db, function(item) {
      res.render('sua', { title: 'Sửa dữ liệu', data: item });
      //console.log(item);
      client.close();  
      
    });
  //res.render('sua', { title: 'Sửa dữ liệu' });
  });
});

/* POST Sua page. */
router.post('/sua/:id', function(req, res, next) {
  var id = MongoObjectId(req.params.id);
  var dulieu = {
    "ten": req.body.ten,
    "sdt" : req.body.sdt
  };

  const updateContact = function(db, callback) {
    // Get the documents collection
    const collection = db.collection('nguoidung');
    // Insert some documents
    collection.updateOne({ _id : id }
      , { $set: { ten : dulieu.ten, sdt : dulieu.sdt } }, function(err, result) {
      assert.equal(err, null);      
      console.log("sua du lieu thanh cong");
      callback(result);
    });
  }

  MongoClient.connect(url, function(err, client) {
    assert.equal(null, err);
    console.log("Connected successfully to server");
  
    const db = client.db(dbName);
  
    updateContact(db, function() {
      client.close();
    });
  });

  res.redirect('/trang-chu');
});

module.exports = router;
